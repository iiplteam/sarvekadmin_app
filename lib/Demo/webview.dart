import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as hp;

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';

class AboutUsWeb extends StatefulWidget {
  @override
  _AboutUsWebState createState() => new _AboutUsWebState();
}

class _AboutUsWebState extends State<AboutUsWeb> {
  InAppWebViewController webView;
  static const headers = {'Content-Type': 'application/json'};
  Future<String> getLink() async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization':
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDgxNjUyMTI1NiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgyMjIyMn0.GranSJeDYtwgvfW0oZuMf-ZBKQNbQWjEWv8YHZa5Ia8"
    };

    var request = hp.Request(
      'GET',
      Uri.parse("CREATE_SURVEY_URL"),
    );
    request.headers.addAll(headers);

    hp.StreamedResponse response = await request.send();

    print(response.statusCode);
    var resp = json.decode(await response.stream.bytesToString());
    print(resp);
    if (response.statusCode == 200) {
      print("survey respone--------$resp['data']");
      url = resp;
      return resp;
    } else {
      print(response.reasonPhrase);
      return resp;
    }
  }

  String url = "";
  @override
  void initState() {
    // TODO: implement initState
    getLink();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: new IconButton(
        //   icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
        //   onPressed: () => scaffoldKey?.currentState?.openDrawer(),
        // ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "View",
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 0)),
        ),
      ),
      body: SafeArea(
        child: Center(
          child: InAppWebView(
            initialUrlRequest:
                URLRequest(url: Uri(path: Constants.baseUrl + url)),
            // initialHeaders: {},
            initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                    // cacheEnabled: true,
                    //debuggingEnabled: true,
                    )),
            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            // onLoadStart: (InAppWebViewController controller, String url) {
            //   setState(() {
            //     this.iframeUrl = url;
            //   });
            // },
            // onLoadStop: (InAppWebViewController controller, String url) async {
            //   setState(() {
            //     this.iframeUrl = url;
            //   });
            // },
            // onProgressChanged: (InAppWebViewController controller, int progress) {
            //   setState(() {
            //     this.progress = progress / 100;
            //   });
            // },admin@123 9988776655
          ),
        ),
      ),
    );
  }
}
