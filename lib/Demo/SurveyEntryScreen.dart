
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';




import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



import 'package:fluttertoast/fluttertoast.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:grouped_checkbox/grouped_checkbox.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:sarvekadmin_app/AppDrawer.dart';
import 'package:sarvekadmin_app/Demo/DemoScreen.dart';
import 'package:sarvekadmin_app/models/Answer.dart';
import 'package:sarvekadmin_app/models/Question.dart';
import 'package:sarvekadmin_app/models/Questiononline.dart';
import 'package:sarvekadmin_app/models/Survey.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';
import 'package:sarvekadmin_app/questions/AddQuestions.dart';
import 'package:sarvekadmin_app/surveypages/AddSurvey.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
import 'dart:io' show Platform;



//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';








class SurveyEntryScreen  extends StatefulWidget
{
  int count;
String surveyId;
SurveyEntryScreen({this.surveyId:"034c3d77-1899-4e2b-a799-4e4efa9b782e"});
  @override
  _SurveyEntryScreenState createState() => _SurveyEntryScreenState();

  void onItemSelected(int position) {
    Fluttertoast.showToast(msg: "your select $position");

  }
}



class _SurveyEntryScreenState extends State<SurveyEntryScreen>  with WidgetsBindingObserver {
  SharedPreferences prefs = null;
  final GlobalKey _scaffoldKey = new GlobalKey();
  SurveyProvider surveyProvider;
  bool intialdataload=true;
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    print("Activity Resumed ");


  }


  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("Activity Resumed $state");
    switch (state.index) {
      case 0: // resumed
      //datasyncwithserver();
        break;
      case 1: // inactive

        break;
      case 2: // paused

        break;
    }
  }
loadData(){
    if(intialdataload) {
      //034c3d77-1899-4e2b-a799-4e4efa9b782e
      surveyProvider.getQuestions(widget.surveyId).then((value){
        for(var i=0;i<surveyProvider.questionsbysurvey.length;i++)
        {
          surveyProvider.addAnswer(i, null);
        }
      });

      setState(() {
        intialdataload=false;
      });
    }



}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    surveyProvider=Provider.of<SurveyProvider>(context);
    loadData();
    return Scaffold(
key: _scaffoldKey,
drawer:
 AppDrawer(),
      endDrawer: AddSurveyPage(),


      body: body(context),
    );
  }

  Widget body(BuildContext context1){
    return Builder(
        builder: (context) =>Column(
      children: [
        SizedBox(height: 50,),
        Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Active Surveys",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.grey[600]),),
              surveyProvider.questionsbysurvey.length>0?Padding(
                padding: const EdgeInsets.only(left:30.0,top: 30),
                child: Container(

                  alignment: Alignment.centerRight,
                  child:
                  RaisedButton(
                      onPressed: (){
                        print(surveyProvider.answermap);
                        List<Answer> sss=new List<Answer>();
                        surveyProvider.answermap.forEach((key,element) {
                          print("..$key..${element.toJson()}");
                          sss.add(element);
                        });
                        print(json.encode(sss));
                        surveyProvider.createAnswer(widget.surveyId,sss).then((value){
                          if(value)
                          {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>SurveyEntryScreen(surveyId: widget.surveyId,)));

                          }else
                          {
                            Fluttertoast.showToast(msg: 'Create answers faild');
                          }
                        });
                      },
                      child: Text('Submit',style: TextStyle(),)),
                ),
              ):Container(),
            ],),
        ),
        SizedBox(height: 50,),
        surveyProvider.questionsbysurvey.length>0?Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left:20.0),
            child: ListView.builder(
                itemCount: surveyProvider.questionsbysurvey.length,
                itemBuilder: (BuildContext context,int index){
                  Questiononline q=surveyProvider.questionsbysurvey[index];
                  return q.questionType=='text'?Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.only(left: 10.0, top: 14.0),
                    child: TextFormField(
                      style:  TextStyle(
                        // fontSize: 18,
                        fontFamily: 'montserrat',
                        // fontWeight: FontWeight.bold

                      ),
                      //controller: ,
                      onChanged: (s){
                        surveyProvider.addAnswer(index, s);
                      },
                      keyboardType: q.validationType=="Number"?TextInputType.number:TextInputType.text,
                      key: Key("0"),
                      decoration:
                      InputDecoration(labelText: q.question,
                          hintText: q.hint,

                          labelStyle: TextStyle(
                          fontSize: 18,
                          fontFamily: 'montserrat',
                          fontWeight: FontWeight.bold

                      )),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'This field is Mandatory ';
                        }else if(q.validationType=="Email")
                          {
                            String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
                                "\\@" +
                                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                                "(" +
                                "\\." +
                                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                                ")+";
                            RegExp regExp = new RegExp(p);

                            if (regExp.hasMatch(value)) {
                              // So, the email is valid
                              return null;
                            }

                            // The pattern of the email didn't match the regex above.
                            return 'Email is not valid';
                          }else
                            {
                              return null;
                            }
                      },
                    ),
                  ):q.questionType=="radio"?getRadioTypeQuestion(q.question,q.options.join("#"),index):
                  q.questionType=="checkbox"?getCheckTypeQuestion(q.question,q.options.join("#"),index,new List<String>(),new TextEditingController()):Text('${q.question}');
                }
            ),
          ),

        ):CircularProgressIndicator(),
      ],
    ));
  }
  Widget getRadioTypeQuestion(String question, String options, int index) {
    List<int> count = List<int>();

    bool sta = false;
    var s = [];
    print("-------------------$index---------------");
    s = options.split("#");

    do {
      sta = s.remove("@");
    } while (sta);
    bool _picked;
    // int z = int.parse(adhocAnswers[ansid]);


//    List<int> valueArray = List<int>();
//    for (var i = 0; i <= s.length - 1; i++) {
//      valueArray.add(i);
//      count.add(i);
//    }

//    print('hhhhhhhhhhhhhh${adhocAnswers[index]}');
//    if (adhocAnswers[index] != "-1") {
//      _picked = adhocAnswers[ansid];
//      print('hhhhhhhhhhhhhhcccccccccc$_picked}');
//    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
              children: <Widget>[
//              Text('Q${index + 1}.',
//                textAlign: TextAlign.start,
//                style: TextStyle(
//                    fontSize: 18,
//                    fontFamily: 'montserrat',
//                    fontWeight: FontWeight.bold),
//              ),
//              SizedBox(width: 10,),
                Container(
                  //width: MediaQuery.of(context).size.width/1.3,
                  child: Text(
                    question,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'montserrat',
                        fontWeight: FontWeight.bold

                    ),
                  ),
                )
              ]),
          // print('bbbbbbbb${adcQuestionsList[index].qid}');
//            adhocAnswers[ansid] = selected;
//            print(selected);
//            print(adhocAnswers);
//            submitAdhocAnswers[ansid] = selected;
          SizedBox(
            height: 5,
          ),
          RadioButtonGroup(
            orientation: GroupedButtonsOrientation.VERTICAL,
            margin: const EdgeInsets.only(left: 12.0),
            onSelected: (String selected) {

              surveyProvider.addAnswer(index, selected);
              setState(() {

              });

            },
            labels: s,
           picked:surveyProvider.answermap[index].value,
            itemBuilder: (Radio rb, Text txt, int i) {
              return Row(
                children: <Widget>[
                  //  Icon(Icons.public),
                  rb,
                  txt,
                ],
              );
            },
          ),

        ],
      ),
    );
  }
  Widget getCheckTypeQuestion(String question, String options, index,List<String> answers,TextEditingController otheranwers ) {
//    String ansid = adcQuestionsList[index].qid;
//    checkAnswer = " ";
    // print(ansid);
    List<String> scheck = List<String>();
    scheck = options.split("#");
    bool sta=false;
    do {
      sta = scheck.remove("@");
    } while (sta);
List<String> sl= surveyProvider.answermap[index].value==null?new List<String>():surveyProvider.answermap[index].value.split('#');
print('bbbbb${surveyProvider.answermap[index].value}bbbbb$sl');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Text(
          question,
          style: TextStyle(
              fontSize: 14,
              fontFamily: 'montserrat',
              fontWeight: FontWeight.bold

          ),
        ),
        SizedBox(
          height: 5,
        ),
        CheckboxGroup(
          orientation: GroupedButtonsOrientation.VERTICAL,
          margin: const EdgeInsets.only(left: 12.0),
          onChange: (state,s,ins){
            setState(() {
              sl.remove('');
            if(state)
            {
              sl.add(s);

            }else{
              sl.remove(s);

            }
            print('ddddddd R$sl');
           surveyProvider.addAnswer(index, sl.join('#'));


           });
          },


          labels: scheck,
          checked:sl,
          itemBuilder: (Checkbox cb, Text txt, int i){
            return  Row(
              children: <Widget>[

                cb,
                Container(
                    //width: MediaQuery.of(context).size.width/1.3,

                    child:txt),
              ],

            );
          },
        ),


      ],
    );
  }

}








