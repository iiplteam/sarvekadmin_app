import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as hp;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../providers/Constants.dart';

class FormApi extends StatefulWidget {
  int survekId;
  FormApi({Key key, this.survekId}) : super(key: key);

  @override
  _FormApiState createState() => _FormApiState();
}

class _FormApiState extends State<FormApi> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  static const headers = {'Content-Type': 'application/json'};

  static Future postRequestToken(
    var data,
    String endPoint,
  ) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = "";
    token = await prefs.getString('token');
    print(token);
    var headers = {
      'Content-Type': 'application/json',
      'Authorization':
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDgxNjUyMTI1NiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgyMjIyMn0.GranSJeDYtwgvfW0oZuMf-ZBKQNbQWjEWv8YHZa5Ia8"
    };
    Map res = {};
    print(
        "apapappapap api url for  add $data   " + Constants.baseUrl + endPoint);
    await hp
        .post(Constants.baseUrl + endPoint, body: data, headers: headers)
        .then((response) {
      print("after post  http" + response.body);
      if (response != null) {
        res = json.decode(response.body);
        // print("after decode" + res.toString());

        if (response.statusCode == 400) {
          //status-1
          // print("insode satus not 400");
          // Fluttertoast.showToast(
          //   msg: res["message"],
          //   backgroundColor: Colors.grey[400],
          //   toastLength: Toast.LENGTH_LONG,
          //   gravity: ToastGravity.BOTTOM,
          //   timeInSecForIosWeb: 2,
          // );

          res = null;
        } //
      }
    }).catchError((error) {
      print(error.toString());
      // Fluttertoast.showToast(
      //   msg: "Server is not responding!!!",
      //   backgroundColor: Colors.grey[400],
      //   toastLength: Toast.LENGTH_LONG,
      //   gravity: ToastGravity.BOTTOM,
      //   timeInSecForIosWeb: 2,
      // );
      res = null;
    });
    // print("at end" + res.toString());
    return res;
    //
  }

  TextEditingController emailSubject;
  TextEditingController emailBody;
  @override
  void initState() {
    // if(editIndex)
    emailSubject = TextEditingController();
    emailBody = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    emailSubject.dispose();
    emailBody.dispose();
  }

  showLoading() => setState(() {
        _isLoading = true;
      });
  stopLoading() => setState(() {
        _isLoading = false;
      });
  bool _isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SingleChildScrollView(
        child: Consumer<SurveyProvider>(builder: (context, survey, ch) {
      return Form(
        key: _form,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Container(
                width: (MediaQuery.of(context).size.width / 100) * 75,
                alignment: Alignment.topCenter,
                padding: EdgeInsets.only(top: 50),
                child: TextFormField(
                  expands: false,
                  validator: (s) {
                    if (s.isNotEmpty) {
                      return null;
                    } else {
                      return "Please Enter some Subject";
                    }
                  },
                  controller: emailSubject,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: "Enter Subject",
                    counterStyle: TextStyle(
                      height: double.minPositive,
                    ),
                    counterText: "",
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Theme.of(context).primaryColor),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).textTheme.title.color),
                    ),
                  ),
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
            ),
            Center(
              child: Container(
                width: (MediaQuery.of(context).size.width / 100) * 75,
                alignment: Alignment.topCenter,
                padding: EdgeInsets.only(top: 25),
                // width: SizeConfig.w * 0.93,
                child: TextFormField(
                  // maxLength: 10,
                  maxLines: 15,
                  controller: emailBody,
                  keyboardType: TextInputType.multiline,
                  validator: (s) {
                    if (s.isNotEmpty) {
                      return null;
                    } else {
                      return "Please Enter some text";
                    }
                  },
                  // maxLength: 10,

                  decoration: InputDecoration(
                    labelText: "Enter Body",

                    // helperText: "Enter your 10 digit Mobile Number",
                    // helperStyle: TextStyle(
                    //     color: Colors.black45, fontWeight: FontWeight.w400),
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Theme.of(context).primaryColor),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context).textTheme.title.color),
                    ),
                  ),
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
            ),
            Center(
              child: _isLoading
                  ? Center(
                      child: Container(
                          margin: EdgeInsets.only(
                              top: (MediaQuery.of(context).size.width / 100) *
                                  0.016,
                              bottom: 10),
                          padding: EdgeInsets.all(5),
                          child: CircularProgressIndicator()),
                    )
                  : GestureDetector(
                      // splashColor: Theme.of(context).primaryColor,
                      onTap: () async {
                        if (_form.currentState.validate()) {
                          showLoading();
                          // await Future.delayed(
                          //     Duration(milliseconds: 600));

                          var headers = {
                            'Authorization':
                                'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDQ4MzMzMTA4MiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgxMjIyN30.ZvKNAbW_jHYESwQKgg2YrjxgN9LgLR9QBtTrL3PxDek',
                            'Content-Type': 'application/json'
                          };
                          var request = hp.Request(
                              'POST',
                              Uri.parse(
                                  'https://arb0853we1.execute-api.ap-southeast-1.amazonaws.com/dev/api/survey/${survey.surveyList[widget.survekId].surveyId}/crm-email'));
                          request.body =
                              '''{\r\n    "subject":${emailSubject.text},\r\n    "html":"<b>${emailBody.text}</b>"\r\n}''';
                          request.headers.addAll(headers);
                          hp.StreamedResponse response = await request.send();

                          print(response.statusCode);

                          // var res = json
                          //     .decode(await response.stream.bytesToString());
                          // Map data = {
                          //   "subject": emailSubject.text,
                          //   "html": "<b>${emailBody.text}</b>"
                          // };

                          // Map res = await postRequestToken(
                          //   json.encode(data),
                          //   "survey//crm-email",
                          // );
                          if (response.statusCode == 200) {
                            //  Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(
                            //                                       "successfully added to cart")));
                            print(" status is true");

                            // FocusScope.of(context).unfocus();
                            // _scaffoldkey.currentState?.showSnackBar(
                            //   SnackBar(
                            //       duration: Duration(seconds:15),
                            //       backgroundColor:
                            //       Theme.of(context).primaryColor,
                            //       content: Text(res["message"])),
                            // );
                            stopLoading();

                            Fluttertoast.showToast(
                                msg: "Email sent Successfully");
                            emailBody.clear();
                            emailSubject.clear();
                          } else {
                            stopLoading();

                            // _scaffoldkey.currentState?.showSnackBar(
                            //   SnackBar(
                            //       duration: Duration(seconds: 15),
                            //       backgroundColor:
                            //       Theme.of(context).primaryColor,
                            //       content: Text(res["message"] ??
                            //           "Sorry, Email not sent!!")),
                            // );
                          }
                        } //form validate
                        // else {
                        //   Fluttertoast.showToast(
                        //       msg: "Please enter the details");
                        // }
                      },
                      child: Container(
                          margin: EdgeInsets.only(top: 100, bottom: 10),
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          height: 50,
                          width: (MediaQuery.of(context).size.width / 100) * 50,
                          child: Center(
                            child: FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(
                                "SEND MAIL",
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    .copyWith(color: Colors.white),
                              ),
                            ),
                          )),
                    ),
            ),
          ],
        ),
      );
    })));
  }
}
