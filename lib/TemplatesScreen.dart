
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';




import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:sarvekadmin_app/AppDrawer.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
import 'dart:io' show Platform;



//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';








class TemplatesScreen  extends StatefulWidget
{
  int count;

  @override
  _TemplatesScreenState createState() => _TemplatesScreenState();

  void onItemSelected(int position) {
    Fluttertoast.showToast(msg: "your select $position");

  }
}



class _TemplatesScreenState extends State<TemplatesScreen>  with WidgetsBindingObserver {
  SharedPreferences prefs = null;
  final GlobalKey _scaffoldKey = new GlobalKey();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    print("Activity Resumed ");

  }


  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("Activity Resumed $state");
    switch (state.index) {
      case 0: // resumed
      //datasyncwithserver();
        break;
      case 1: // inactive

        break;
      case 2: // paused

        break;
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
key: _scaffoldKey,
drawer:
 AppDrawer(),


      body: ScreenTypeLayout(
          mobile: body(),

      desktop:Row(children: [
        SizedBox(
            width: 300,
            child: AppDrawer(selectedIndex: 1,)),
        Expanded(child: body())
      ],),

    ),
    );
  }

  Widget body(){
    return Padding(
      padding: const EdgeInsets.only(left:20.0),
      child: Column(
        children: [
          SizedBox(height: 50,),
          Container(
            padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              child: Text("Templates",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Theme.of(context).focusColor),)),
          Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              child: Text("Sales",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,),)),
          Container(
            height: 150,
            child: Padding(
                padding: const EdgeInsets.only(left:0.0),
                child: ListView.builder(
                 scrollDirection: Axis.horizontal,
                    itemCount: 10,
                    itemBuilder: (BuildContext ctx, index) {
                      return  Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: EdgeInsets.all(10),
width: 200,
                          alignment: Alignment.bottomLeft,
                          decoration: BoxDecoration(
                           // color: Colors.white,
                            image: DecorationImage(image: AssetImage("asserts/template.png"),fit: BoxFit.fill),
                            borderRadius: BorderRadius.all(Radius.circular(22),),

                          ),
                          child: Text("Sarvek$index", style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.white),),


                        ),
                      );
                    }),
              ),
          ),


          Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              child: Text("Products",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,),)),
          Container(
            height: 150,
            child: Padding(
              padding: const EdgeInsets.only(left:0.0),
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 10,
                  itemBuilder: (BuildContext ctx, index) {
                    return  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: 200,
                        alignment: Alignment.bottomLeft,
                        decoration: BoxDecoration(
                          // color: Colors.white,
                          image: DecorationImage(image: AssetImage("asserts/template.png"),fit: BoxFit.fill),
                          borderRadius: BorderRadius.all(Radius.circular(22),),

                        ),
                        child: Text("Sarvek$index", style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.white),),


                      ),
                    );
                  }),
            ),
          ),


          Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              child: Text("Marketing",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,),)),
          Container(
            height: 150,
            child: Padding(
              padding: const EdgeInsets.only(left:0.0),
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 10,
                  itemBuilder: (BuildContext ctx, index) {
                    return  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: 200,
                        alignment: Alignment.bottomLeft,
                        decoration: BoxDecoration(
                          // color: Colors.white,
                          image: DecorationImage(image: AssetImage("asserts/template.png"),fit: BoxFit.fill),
                          borderRadius: BorderRadius.all(Radius.circular(22),),

                        ),
                        child: Text("Sarvek$index", style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.white),),


                      ),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}








