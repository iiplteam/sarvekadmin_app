
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';


import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/models/User.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';

import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'User-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  static final CREATE_USER_URL = Constants.baseUrl+'user';
  TextEditingController emailControler = new TextEditingController();
  TextEditingController usertypeControler = new TextEditingController();
  TextEditingController phoneControler = new TextEditingController();
  TextEditingController nameControler = new TextEditingController();

  SharedPreferences  prefs=null;
  var  userId="";
  TextEditingController mobile=new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    _getshared_pre();


  }
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: Container(
       // width: MediaQuery.of(context).size.width/2,
       // height: MediaQuery.of(context).size.height/,
        /// Set Background image in splash screen layout (Click to open code)

        child: Image.asset("asserts/logo.png",width: 200,height: 50,),
      ),
    );









    return Scaffold(

        body:  Container(
           // alignment: Alignment.centerRight,
          height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('asserts/bg.png'), fit: BoxFit.cover),
           ),
            child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Padding(
                  padding: const EdgeInsets.only(left:30.0,top: 30),
                  child: Container(

                      alignment: Alignment.centerLeft,
                      child: logo),
                ),
                Expanded(
                  child: Form(
                    key: _formKey,
                    child: new Center(
                      child: Container(
                        margin: const EdgeInsets.all(15.0),
                        padding: const EdgeInsets.all(3.0),
                        // decoration: BoxDecoration(
                        //    // color: Colors.white,
                        //     border: Border.all(color:Theme.of(context).focusColor ),
                        //     borderRadius:  BorderRadius.circular(40)
                        // ),
                        width: 350,
                        alignment:Alignment.centerRight ,
                        child: ListView(
                          shrinkWrap: true,

                          children: <Widget>[
                            Container(

                              child: Text("Sign Up",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white),),
                              padding: EdgeInsets.all(20),),
                            SizedBox(height: 20,),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: Text('Email Address',style: TextStyle(fontSize: 14,color: Colors.white70),),
                              ),
                            ),
                            SizedBox(height: 20,),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: TextFormField(
                                  controller: emailControler,
                                  keyboardType: TextInputType.emailAddress,
                                  autofocus: false,
                                  validator: _validateEmail,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                    //   labelText: "Email",
                                      hintText: "Enter Email",
                                      hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                      isDense: true, // important line
                                      contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.white,
                                          ),
                                          borderRadius: BorderRadius.circular(40)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color:Theme.of(context).focusColor,
                                          ),
                                          borderRadius: BorderRadius.circular(40)),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(color: Colors.white))),



                                ),
                              ),
                            ),
                            SizedBox(height: 20.0),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: Text('Name',style: TextStyle(fontSize: 14,color: Colors.white70),),
                              ),
                            ),
                            SizedBox(height: 20,),
                            Padding(
                              padding: const EdgeInsets.only(left: 20,right: 20),
                              child: TextFormField(
                                controller: nameControler,
                                keyboardType: TextInputType.name,
                                autofocus: false,
                                validator: (s){
                                  if(s.trim().isEmpty)
                                    {
                                      return "Please enter name";
                                    }
                                  return null;
                                },
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  //   labelText: "Email",
                                    hintText: "Enter name",
                                    hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                    isDense: true, // important line
                                    contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color:Theme.of(context).focusColor,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(color: Colors.white))),



                              ),
                            ),
                            SizedBox(height: 20.0),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: Text('Phone',style: TextStyle(fontSize: 14,color: Colors.white70),),
                              ),
                            ),
                            SizedBox(height: 20,),
                            Padding(
                              padding: const EdgeInsets.only(left: 20,right: 20),
                              child: TextFormField(
                                controller: phoneControler,
                                keyboardType: TextInputType.phone,
                                autofocus: false,
                                validator: (s){
                                  if(s.trim().isEmpty)
                                    {
                                      return "Plase enter phone number";
                                    }else if(s.length!=10)
                                      {
                                        return "Plase enter valid phone number";
                                      }
                                  return null;
                                },
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  //   labelText: "Email",
                                    hintText: "Enter Phone",
                                    hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                    isDense: true, // important line
                                    contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color:Theme.of(context).focusColor,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(color: Colors.white))),



                              ),
                            ),
                            SizedBox(height: 20.0),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: Text('UserType',style: TextStyle(fontSize: 14,color: Colors.white70),),
                              ),
                            ),
                            SizedBox(height: 20,),
                            Padding(
                              padding: const EdgeInsets.only(left: 20,right: 20),
                              child: TextFormField(
                                controller: usertypeControler,
                                keyboardType: TextInputType.name,
                                autofocus: false,
                                validator: (s){
                                  if(s.trim().isEmpty)
                                  {
                                    return "Plase enter user type";
                                  }
                                  return null;
                                },
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  //   labelText: "Email",
                                    hintText: "Enter user type",
                                    hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                    isDense: true, // important line
                                    contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color:Theme.of(context).focusColor,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(color: Colors.white))),



                              ),
                            ),
                            SizedBox(height: 20.0),


                            Padding(
                              padding: const EdgeInsets.only(left: 20, right: 20),
                              child:  RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                onPressed: ()  async {
                                  if (_formKey.currentState.validate() ) {
                                    try {
                                      var body = json.encode({"name":nameControler.text,"email":emailControler.text,"password":phoneControler.text,"role":usertypeControler.text});
                                      print("${body}");
                                      var response = await http.post(Uri.parse(CREATE_USER_URL),
                                          headers: {"Content-Type": "application/json"},
                                          body: body
                                      );
                                      print("${response.statusCode}");
                                      print("${response.body}");
if(response.statusCode==201)
  {
    var resp=json.decode(response.body);
    if(resp['status'])
      {
        Navigator.pop(context);
      }else{
      Fluttertoast.showToast(msg: "Please try again");

    }
  }else{
  Fluttertoast.showToast(msg: "Please try again");

}
                                      return response.body.toString();
                                    }
                                    on SocketException catch (_) {
                                      Fluttertoast.showToast(msg: "Check your Internet Connections");
                                    }
                                  }
                                },
                                padding: EdgeInsets.all(18),
                                color: Theme.of(context).focusColor,
                                child: Text('Submit', style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.bold)),
                              ),
                            ),

                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
  }
  Future<String> createPost(String url, body) async {
    print(body);
    return http.post(Uri.parse(url),headers: {"Content-Type": "application/json"}, body: body).then((http.Response response) {
      final int statusCode = response.statusCode;
      print(response.body);

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return response.body;
    });
  }
  Future<String> apiRequest(String url, String email,String password) async {
    // print(jsonMap);

    var body = json.encode({"name":email,"password":password});

    var response = await http.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: body
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }

  _getshared_pre() async {
    prefs=await SharedPreferences.getInstance();
    userId = prefs.getString("email") ?? '';
  }
  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
    //return null;
  }

//  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context,int index) async {
//    return showDialog<ConfirmAction>(
//        context: context,
//        barrierDismissible: false, // user must tap button for close dialog!
//        builder: (BuildContext context) {
//          return AlertDialog(
//            title: Text('Reset  password'),
//            content: TextFormField(style: TextStyle(fontFamily: 'Gilroy'),
//              controller: mobile,
//              keyboardType: TextInputType.phone,
//              decoration: InputDecoration(
//                  labelText: 'Enter Mobile Number'
//              ),
//            ),
//            actions: <Widget>[
//              FlatButton(
//                padding: EdgeInsets.all(10),
//                child: const Text('OK', style: TextStyle(
//                    backgroundColor: Colors.red,
//                    color: Colors.white,
//                    fontSize: 20,fontFamily: 'Gilroy'),),
//                onPressed: () {
//                  forgotRequest().then((onValue){
//                    if(onValue.contains("true"))
//                      {
//                        Fluttertoast.showToast(msg: "Password sent successful");
//                      }
//                      else
//                        {
//                          Fluttertoast.showToast(msg: "Invalid phone number entered");
//                        }
//                  });
//                  Navigator.pop(context);
//
//                },
//              ),
//
//
//
//            ],
//          );
//        });
//  }

  Future<String> forgotRequest() async {


    var bodyre="{\"phone\":${mobile.text}}";


    var response = await http.post(Uri.parse("http://pioneer.redmatter.tech/pioneer/web/forgot_password.php"),
        headers: {"Content-Type": "application/json"},
        body: bodyre
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }
}

