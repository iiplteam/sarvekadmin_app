import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sarvekadmin_app/Demo/DemoScreen.dart';
import 'package:sarvekadmin_app/Demo/SurveyEntryScreen.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/login.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';
import 'package:sarvekadmin_app/questions/AddQuestions.dart';
import 'package:sarvekadmin_app/questions/GroupSingleView.dart';
import 'package:sarvekadmin_app/questions/QuestionSingleView.dart';
import 'package:sarvekadmin_app/surveypages/AddSurvey.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SurveyProvider()),
      ],
      child: MaterialApp(
        title: 'Sarvek',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.indigo,
          primaryColor: Color(0xFF39396D),
          focusColor: Colors.yellow[900]
        ),
        home:HomeScreen(),
      ),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

/// Component UI
class _SplashScreenState extends State<SplashScreen> {

  /// Check user
  bool _checkUser = true;


  SharedPreferences prefs;

  Future<Null> _function() async {
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    this.setState(() {
      if (prefs.getString("username") != null) {
        print('false');
        _checkUser = false;
      } else {
        print('true');
        _checkUser = true;
      }
    });
  }


  @override
  /// Setting duration in splash screen
  startTime() async {
    return new Timer(Duration(milliseconds: 1000), NavigatorPage);
  }
  /// To navigate layout change
  void NavigatorPage() async{
    Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> LoginPage()));
    if(_checkUser){
      /// if userhas never been login
     // Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> LoginPage()));
    }else{
      /// if userhas ever been login
      SharedPreferences prefs;
      prefs = await SharedPreferences.getInstance();
      print('bbbbbbbbbbbbbbbbbbbbbb${prefs.getString("profiledata")}');
      if(prefs.getString("profiledata")!=null)
      {
       // Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> bottomNavigationBar(currentIndex: 0,)));

      }else{
      //  Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> loginScreen()));
      }
    }
  }
  /// Declare startTime to InitState
  @override
  void initState() {
    super.initState();
    startTime();
    _function();
  }
  /// Code Create UI Splash Screen
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.indigo,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        /// Set Background image in splash screen layout (Click to open code)
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('asserts/bg.png'), fit: BoxFit.cover)),
        child: Image.asset("asserts/logo.png"),
      ),
    );
  }
}
