class Answer {
  String surveyCode;
  String questionCode;
  String value;
  String question;
  String valueType;
  String answerBy;

  Answer({this.surveyCode, this.questionCode, this.value,this.question, this.valueType,this.answerBy});

  Answer.fromJson(Map<String, dynamic> json) {
    surveyCode = json['surveyCode'];
    questionCode = json['questionCode'];
    value = json['value'];
    question = json['question'];
    valueType=json['valueType'];
    answerBy = json['answerBy'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['surveyCode'] = this.surveyCode;
    data['questionCode'] = this.questionCode;
    data['value'] = this.value;
    data['question'] = this.question;
    data['valueType']=this.valueType;
    data['answerBy'] = this.answerBy;
    return data;
  }
}
