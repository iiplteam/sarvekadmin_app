class Questiononline {
  String question;
  bool isDelete;
  int index;
  String questionType;
  String subQuestions;
  String createdBy;
  String groupName;
  int updatedOn;
  String questionCode;
  bool isMandatory;
  String validation;
  String hint;
  String questionId;
  String surveyCode;
  List<String> options;
  String validationType;
  int createdOn;

  Questiononline(
      {this.question,
        this.isDelete,
        this.index,
        this.questionType,
        this.subQuestions,
        this.createdBy,
        this.groupName,
        this.updatedOn,
        this.questionCode,
        this.isMandatory,
        this.validation,
        this.hint,
        this.questionId,
        this.surveyCode,
        this.options,
        this.validationType,
        this.createdOn});

  Questiononline.fromJson(Map<String, dynamic> json) {
    question = json['question'];
    isDelete = json['isDelete'];
    index = json['index'];
    questionType = json['questionType'];
    subQuestions = json['subQuestions'];
    createdBy = json['created_by'];
    groupName = json['groupName'];
    updatedOn = json['updated_on'];
    questionCode = json['questionCode'];
    isMandatory = json['isMandatory'];
    validation = json['validation'];
    hint = json['hint'];
    questionId = json['question_id'];
    surveyCode = json['surveyCode'];
    options = json['options']=="N/A"?[]:json['options'].cast<String>();
    validationType = json['validationType'];
    createdOn = json['created_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    data['isDelete'] = this.isDelete;
    data['index'] = this.index;
    data['questionType'] = this.questionType;
    data['subQuestions'] = this.subQuestions;
    data['created_by'] = this.createdBy;
    data['groupName'] = this.groupName;
    data['updated_on'] = this.updatedOn;
    data['questionCode'] = this.questionCode;
    data['isMandatory'] = this.isMandatory;
    data['validation'] = this.validation;
    data['hint'] = this.hint;
    data['question_id'] = this.questionId;
    data['surveyCode'] = this.surveyCode;
    data['options'] = this.options;
    data['validationType'] = this.validationType;
    data['created_on'] = this.createdOn;
    return data;
  }
}
