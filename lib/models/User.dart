class User {
  bool status;
  List<Data> data;
  String token;

  User({this.status, this.data, this.token});

  User.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['token'] = this.token;
    return data;
  }
}

class Data {
  String userId;
  bool isDelete;
  int updatedOn;
  String email;
  String name;
  int createdOn;

  Data(
      {this.userId,
        this.isDelete,
        this.updatedOn,
        this.email,
        this.name,
        this.createdOn});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    isDelete = json['is_delete'];
    updatedOn = json['updated_on'];
    email = json['email'];
    name = json['name'];
    createdOn = json['created_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['is_delete'] = this.isDelete;
    data['updated_on'] = this.updatedOn;
    data['email'] = this.email;
    data['name'] = this.name;
    data['created_on'] = this.createdOn;
    return data;
  }
}
