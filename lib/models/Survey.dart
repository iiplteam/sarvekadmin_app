class Survey {
  String surveyName;
  bool isDelete;
  String surveyId;
  int endDate;
  String createdBy;
  int startDate;
  String surveyCode;
  bool isPublic;
  String group;
  String description;
  int updatedOn;
  int createdOn;

  Survey(
      {this.surveyName,
        this.isDelete,
        this.surveyId,
        this.endDate,
        this.createdBy,
        this.startDate,
        this.surveyCode,
        this.isPublic,
        this.group,
        this.description,
        this.updatedOn,
        this.createdOn});

  Survey.fromJson(Map<String, dynamic> json) {
    surveyName = json['surveyName'];
    isDelete = json['isDelete'];
    surveyId = json['survey_id'];
    //endDate = json['endDate'];
    createdBy = json['created_by'];
   // startDate = json['startDate'];
    surveyCode = json['surveyCode'];
    isPublic = json['isPublic'];
    group = json['group'];
    description=json['description'];
   // updatedOn = json['updated_on'];
   // createdOn = json['created_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['surveyName'] = this.surveyName;
    data['isDelete'] = this.isDelete;
    data['survey_id'] = this.surveyId;
    data['endDate'] = this.endDate;
    data['created_by'] = this.createdBy;
    data['startDate'] = this.startDate;
    data['surveyCode'] = this.surveyCode;
    data['isPublic'] = this.isPublic;
    data['group'] = this.group;
    data['description']=this.description;
    data['updated_on'] = this.updatedOn;
    data['created_on'] = this.createdOn;
    return data;
  }
}
