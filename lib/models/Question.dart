class Question {
  String question;
  String surveyCode;
  String questionCode;
  List<String> options;
  String hint;
  String questionType;
  int index;
  String validationType;
  String groupName;
  bool subQuestions;
  bool isMandatory;
  String validation;

  Question(
      {this.question,
        this.surveyCode,
        this.questionCode,
        this.options,
        this.hint,
        this.questionType,
        this.index,
        this.validationType,
        this.groupName,
        this.subQuestions,
        this.isMandatory,
        this.validation});

  Question.fromJson(Map<String, dynamic> json) {
    question = json['question'];
    surveyCode = json['surveyCode'];
    questionCode = json['questionCode'];
    options = json['options'].cast<String>();
    hint = json['hint'];
    questionType = json['questionType'];
    index = json['index'];
    validationType = json['validationType'];
    groupName = json['groupName'];
    subQuestions = json['subQuestions'];
    isMandatory = json['isMandatory'];
    validation = json['validation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    data['surveyCode'] = this.surveyCode;
    data['questionCode'] = this.questionCode;
    data['options'] = this.options;
    data['hint'] = this.hint;
    data['questionType'] = this.questionType;
    data['index'] = this.index;
    data['validationType'] = this.validationType;
    data['groupName'] = this.groupName;
    data['subQuestions'] = this.subQuestions;
    data['isMandatory'] = this.isMandatory;
    data['validation'] = this.validation;
    return data;
  }
}
