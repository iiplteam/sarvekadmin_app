
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';


import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/models/User.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';

import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  static final LOGIN_URL = Constants.baseUrl+'user/login';
  static final CREATE_USER_URL = Constants.baseUrl+'user';
  TextEditingController emailControler = new TextEditingController();
  TextEditingController passwordControler = new TextEditingController();


  SharedPreferences  prefs=null;
  var  userId="";
  TextEditingController mobile=new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _getshared_pre();


  }
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: Container(
       // width: MediaQuery.of(context).size.width/2,
       // height: MediaQuery.of(context).size.height/,
        /// Set Background image in splash screen layout (Click to open code)

        child: Image.asset("asserts/logo.png",width: 200,height: 50,),
      ),
    );









    return Scaffold(

        body:  Container(
           // alignment: Alignment.centerRight,
          height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('asserts/bg.png'), fit: BoxFit.cover),
           ),
            child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Padding(
                  padding: const EdgeInsets.only(left:30.0,top: 30),
                  child: Container(

                      alignment: Alignment.centerLeft,
                      child: logo),
                ),
                 Expanded(
                  child: Form(
                    key: _formKey,
                    child: new Center(
                      child: Container(
                        margin: const EdgeInsets.all(15.0),
                        padding: const EdgeInsets.all(3.0),
                        // decoration: BoxDecoration(
                        //    // color: Colors.white,
                        //     border: Border.all(color:Theme.of(context).focusColor ),
                        //     borderRadius:  BorderRadius.circular(40)
                        // ),
                        width: 350,
                        alignment:Alignment.centerRight ,
                        child: ListView(
                          shrinkWrap: true,

                          children: <Widget>[
                            Container(
                              
                              child: Text("Login",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white),),
                            padding: EdgeInsets.all(20),),
SizedBox(height: 20,),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: Text('Email Address',style: TextStyle(fontSize: 14,color: Colors.white70),),
                              ),
                            ),
                            SizedBox(height: 20,),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: TextFormField(
                                  controller: emailControler,
                                  keyboardType: TextInputType.emailAddress,
                                  autofocus: false,
                                  validator: _validateEmail,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                    //   labelText: "Email",
                                      hintText: "Enter Email",
                                      hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                      isDense: true, // important line
                                      contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.white,
                                          ),
                                          borderRadius: BorderRadius.circular(40)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color:Theme.of(context).focusColor,
                                          ),
                                          borderRadius: BorderRadius.circular(40)),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(color: Colors.white))),



                                ),
                              ),
                            ),
                            SizedBox(height: 20.0),
                            Container(
                              width: 50,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: Text('Password',style: TextStyle(fontSize: 14,color: Colors.white70),),
                              ),
                            ),
                            SizedBox(height: 20,),
                            Padding(
                              padding: const EdgeInsets.only(left: 20,right: 20),
                              child: TextFormField(
                                controller: passwordControler,
                                autofocus: false,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter password';
                                  }else{
                                    return null;
                                  }
                                },

                                // initialValue: 'some password',
                                obscureText: true,
                                style:TextStyle(color: Colors.white),

                                decoration: InputDecoration(

                                    hintText: "Enter Password",
                                    hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                    isDense: true, // important line
                                    contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Theme.of(context).focusColor,
                                        ),
                                        borderRadius: BorderRadius.circular(40)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(40),
                                        borderSide: BorderSide(color: Colors.white))),

                              ),
                            ),
                            SizedBox(height: 24.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [Text(''),
                                FlatButton(
                                  child: Text(
                                    'Forgot password?',
                                    style: TextStyle(color: Theme.of(context).focusColor,decoration:  TextDecoration.underline),
                                  ),
                                  onPressed: () {
                                    // _asyncConfirmDialog(context,1);
                                  },
                                ),
                              ],
                            ),
                            SizedBox(height: 24.0),

                            Padding(
                              padding: const EdgeInsets.only(left: 20, right: 20),
                              child:  RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                onPressed: ()  async {
                                  if (_formKey.currentState.validate() ) {
                                    try {
                                      // final result = await InternetAddress.lookup('google.com');
                                      // if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                                      //   print('connected');


                                      String s = await apiRequest(LOGIN_URL, emailControler.text.toString(), passwordControler.text.toString());
                                      print(s);
                                      final body = json.decode(s);
                                      if (body['status']) {


// This is how you get success value out of the actual json

                                        //Token is nested inside data field so it goes one deeper.
                                        final String token = body['token'];

                                        final User user = User.fromJson(body);

                                        prefs.setString(
                                            'email', emailControler.text.toString());
                                        prefs.setString('token', token);

                                        prefs.setString("user",jsonEncode(user));
                                        prefs.setInt("start",1);


                                        final myString = prefs.getString('email') ?? '';

                                        Fluttertoast.showToast(msg: "$myString successful ");
                                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>HomeScreen()));




                                      }
                                      else {
                                        Fluttertoast.showToast(msg: "Login Failed Please try Agin");
                                      }
                                      // }
                                    }
                                    on SocketException catch (_) {
                                      Fluttertoast.showToast(msg: "Check your Internet Connections");
                                    }
                                  }
                                },
                                padding: EdgeInsets.all(18),
                                color: Theme.of(context).focusColor,
                                child: Text('Log In', style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.bold)),
                              ),
                            ),
                            SizedBox(height: 20,),



                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
  }
  Future<String> createPost(String url, body) async {
    print(body);
    return http.post(Uri.parse(url),headers: {"Content-Type": "application/json"}, body: body).then((http.Response response) {
      final int statusCode = response.statusCode;
      print(response.body);

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return response.body;
    });
  }
  Future<String> apiRequest(String url, String email,String password) async {
    // print(jsonMap);

    var body = json.encode({"name":email,"password":password});

    var response = await http.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: body
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }

  _getshared_pre() async {
    prefs=await SharedPreferences.getInstance();
    userId = prefs.getString("email") ?? '';
  }
  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
    //return null;
  }

//  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context,int index) async {
//    return showDialog<ConfirmAction>(
//        context: context,
//        barrierDismissible: false, // user must tap button for close dialog!
//        builder: (BuildContext context) {
//          return AlertDialog(
//            title: Text('Reset  password'),
//            content: TextFormField(style: TextStyle(fontFamily: 'Gilroy'),
//              controller: mobile,
//              keyboardType: TextInputType.phone,
//              decoration: InputDecoration(
//                  labelText: 'Enter Mobile Number'
//              ),
//            ),
//            actions: <Widget>[
//              FlatButton(
//                padding: EdgeInsets.all(10),
//                child: const Text('OK', style: TextStyle(
//                    backgroundColor: Colors.red,
//                    color: Colors.white,
//                    fontSize: 20,fontFamily: 'Gilroy'),),
//                onPressed: () {
//                  forgotRequest().then((onValue){
//                    if(onValue.contains("true"))
//                      {
//                        Fluttertoast.showToast(msg: "Password sent successful");
//                      }
//                      else
//                        {
//                          Fluttertoast.showToast(msg: "Invalid phone number entered");
//                        }
//                  });
//                  Navigator.pop(context);
//
//                },
//              ),
//
//
//
//            ],
//          );
//        });
//  }

  Future<String> forgotRequest() async {


    var bodyre="{\"phone\":${mobile.text}}";


    var response = await http.post(Uri.parse("http://pioneer.redmatter.tech/pioneer/web/forgot_password.php"),
        headers: {"Content-Type": "application/json"},
        body: bodyre
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }
}

