
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';


import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/models/Survey.dart';
import 'package:sarvekadmin_app/models/User.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';

import 'package:shared_preferences/shared_preferences.dart';

class AddSurveyPage extends StatefulWidget {
  static String tag = 'User-page';
  @override
  _AddSurveyPageState createState() => new _AddSurveyPageState();
}

class _AddSurveyPageState extends State<AddSurveyPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  static final CREATE_SURVEY_URL = Constants.baseUrl+'survey';
  //TextEditingController codeControler = new TextEditingController();
  TextEditingController nameControler = new TextEditingController();
  TextEditingController startdateControler = new TextEditingController();
  TextEditingController enddateControler = new TextEditingController();
  TextEditingController discriptionControler = new TextEditingController();
  String startdate,enddate,status;
SurveyProvider surveyProvider;
  SharedPreferences  prefs=null;
  bool isPublic=false;
  var  userId="";
  TextEditingController mobile=new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    _getshared_pre();


  }
  @override
  Widget build(BuildContext context) {
    surveyProvider=Provider.of<SurveyProvider>(context);
    final logo = Hero(
      tag: 'hero',
      child: Container(
        // width: MediaQuery.of(context).size.width/2,
        // height: MediaQuery.of(context).size.height/,
        /// Set Background image in splash screen layout (Click to open code)

        child: Image.asset("asserts/logo.png",width: 200,height: 50,),
      ),
    );









    return  Container(
        width: 300,
       // color: Colors.grey[600],
        // alignment: Alignment.centerRight,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('asserts/bg.png'), fit: BoxFit.cover),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

            // Padding(
            //   padding: const EdgeInsets.only(left:30.0,top: 30),
            //   child: Container(
            //
            //       alignment: Alignment.centerLeft,
            //       child: logo),
            // ),
            Expanded(
              child: Form(
                key: _formKey,
                child: new Center(
                  child: Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    // decoration: BoxDecoration(
                    //    // color: Colors.white,
                    //     border: Border.all(color:Theme.of(context).focusColor ),
                    //     borderRadius:  BorderRadius.circular(40)
                    // ),
                    width: 350,
                    alignment:Alignment.centerRight ,
                    child: ListView(
                      shrinkWrap: true,

                      children: <Widget>[
                        Container(

                          child: Text("Create New Survey",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white),),
                          padding: EdgeInsets.all(20),),
                        SizedBox(height: 20,),

                        Container(
                          width: 50,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20,right: 20),
                            child: Text('Name',style: TextStyle(fontSize: 14,color: Colors.white70),),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,right: 20),
                          child: TextFormField(
                            controller: nameControler,
                            keyboardType: TextInputType.name,
                            autofocus: false,
                            validator: (s){
                              if(s.trim().isEmpty)
                              {
                                return "Please enter name";
                              }
                              return null;
                            },
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              //   labelText: "Email",
                                hintText: "Enter name",
                                hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                isDense: true, // important line
                                contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    ),
                                    borderRadius: BorderRadius.circular(40)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color:Theme.of(context).focusColor,
                                    ),
                                    borderRadius: BorderRadius.circular(40)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(color: Colors.white))),



                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          width: 50,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20,right: 20),
                            child: Text('Start Date',style: TextStyle(fontSize: 14,color: Colors.white70),),
                          ),
                        ),
                        SizedBox(height: 20,),
Row(
  children: [
    Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20,right: 20),
        child: TextFormField(
          readOnly: true,
          controller: startdateControler,
          keyboardType: TextInputType.name,
          autofocus: false,
          validator: (s){
            if(s.trim().isEmpty)
            {
              return "Please Select Date";
            }
            return null;
          },
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
            //   labelText: "Email",
              hintText: "Select Date",
              hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
              isDense: true, // important line
              contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(40)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color:Theme.of(context).focusColor,
                  ),
                  borderRadius: BorderRadius.circular(40)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.white))),



        ),
      ),
    ),
        InkWell(

      onTap: (){

        DatePicker.showDateTimePicker(context,

           // showTitleActions: true,

            minTime: DateTime(2018, 3, 5),

            maxTime: DateTime(2019, 6, 7),

            theme: DatePickerTheme(

                headerColor: Colors.orange,

                backgroundColor: Colors.blue,

                itemStyle: TextStyle(

                    color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),

                doneStyle: TextStyle(color: Colors.white, fontSize: 16)),

            onChanged: (date) {

              print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());

            }, onConfirm: (date) {
          setState(() {
            startdateControler.text='$date';
          });

              print('confirm $date');

            }, currentTime: DateTime.now(), locale: LocaleType.en);

      },

        child: Icon(Icons.date_range_sharp,color: Colors.white,)),
  ],
),

                        SizedBox(height: 20.0),
                        //enddate
                        Container(
                          width: 50,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20,right: 20),
                            child: Text('End Date',style: TextStyle(fontSize: 14,color: Colors.white70),),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20,right: 20),
                                child: TextFormField(
                                  readOnly: true,
                                  controller: enddateControler,
                                  keyboardType: TextInputType.name,
                                  autofocus: false,
                                  validator: (s){
                                    if(s.trim().isEmpty)
                                    {
                                      return "Please Select Date";
                                    }
                                    return null;
                                  },
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                    //   labelText: "Email",
                                      hintText: "Select End Date",
                                      hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                      isDense: true, // important line
                                      contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.white,
                                          ),
                                          borderRadius: BorderRadius.circular(40)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color:Theme.of(context).focusColor,
                                          ),
                                          borderRadius: BorderRadius.circular(40)),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(color: Colors.white))),



                                ),
                              ),
                            ),
                            InkWell(

                                onTap: (){

                                  DatePicker.showDateTimePicker(context,

                                      // showTitleActions: true,

                                      minTime: DateTime(2018, 3, 5),

                                      maxTime: DateTime(2019, 6, 7),

                                      theme: DatePickerTheme(

                                          headerColor: Colors.orange,

                                          backgroundColor: Colors.blue,

                                          itemStyle: TextStyle(

                                              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),

                                          doneStyle: TextStyle(color: Colors.white, fontSize: 16)),

                                      onChanged: (date) {

                                        print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());

                                      }, onConfirm: (date) {
                                        setState(() {
                                          enddateControler.text='$date';
                                        });

                                        print('confirm $date');

                                      }, currentTime: DateTime.now(), locale: LocaleType.en);

                                },

                                child: Icon(Icons.date_range_sharp,color: Colors.white,)),
                          ],
                        ),

                        SizedBox(height: 20.0),
                        Container(
                          width: 50,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20,right: 20),
                            child: Text('Description',style: TextStyle(fontSize: 14,color: Colors.white70),),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          width: 50,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20,right: 20),
                            child: TextFormField(
                              controller:discriptionControler,
                              keyboardType: TextInputType.text,
                              autofocus: false,
                              validator: (s){
                                if(s.trim().isEmpty)
                                {
                                  return "Please enter description";
                                }
                                return null;
                              },
maxLines: 2,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                //   labelText: "Email",
                                  hintText: "Enter description",
                                  hintStyle:TextStyle(color: Colors.white60,fontSize: 12) ,
                                  isDense: true, // important line

                                  contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                      borderRadius: BorderRadius.circular(40)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color:Theme.of(context).focusColor,
                                      ),
                                      borderRadius: BorderRadius.circular(40)),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(color: Colors.white))),



                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Row(
                          children: [
                            Checkbox(
                              value: isPublic,

                              onChanged: (bool value) {
                                setState(() {
                                  isPublic= value;
                                });
                              },
                            ),
                            Text('Public',style: TextStyle(fontSize: 18,color: Colors.white),)
                          ],
                        ),
                        SizedBox(height: 20.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child:  RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            onPressed: ()  async {
                              if (_formKey.currentState.validate() ) {
                                Survey survey=new Survey();
                                survey.surveyName=nameControler.text;
                               // survey.surveyCode=codeControler.text;
                                survey.description=discriptionControler.text;
                                survey.startDate=DateTime.parse(startdateControler.text).microsecond;
                                survey.endDate=DateTime.parse(enddateControler.text).microsecond;
                                survey.isPublic=isPublic;
                                surveyProvider.createSurvey(survey).then((value) {
                                  if(value)
                                    {
                                     print('vcvcxvxcvxcvxcvxcvcxvcxv $value');
                                     Navigator.pop(context);
                                    }
                                });
                              }else{
                                Fluttertoast.showToast(msg: "Please enter all Questons");
                              }
                            },
                            padding: EdgeInsets.all(18),
                            color: Theme.of(context).focusColor,
                            child: Text('Submit', style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.bold)),
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
  }



  _getshared_pre() async {
    prefs=await SharedPreferences.getInstance();
    userId = prefs.getString("email") ?? '';
  }
  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
    //return null;
  }

//  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context,int index) async {
//    return showDialog<ConfirmAction>(
//        context: context,
//        barrierDismissible: false, // user must tap button for close dialog!
//        builder: (BuildContext context) {
//          return AlertDialog(
//            title: Text('Reset  password'),
//            content: TextFormField(style: TextStyle(fontFamily: 'Gilroy'),
//              controller: mobile,
//              keyboardType: TextInputType.phone,
//              decoration: InputDecoration(
//                  labelText: 'Enter Mobile Number'
//              ),
//            ),
//            actions: <Widget>[
//              FlatButton(
//                padding: EdgeInsets.all(10),
//                child: const Text('OK', style: TextStyle(
//                    backgroundColor: Colors.red,
//                    color: Colors.white,
//                    fontSize: 20,fontFamily: 'Gilroy'),),
//                onPressed: () {
//                  forgotRequest().then((onValue){
//                    if(onValue.contains("true"))
//                      {
//                        Fluttertoast.showToast(msg: "Password sent successful");
//                      }
//                      else
//                        {
//                          Fluttertoast.showToast(msg: "Invalid phone number entered");
//                        }
//                  });
//                  Navigator.pop(context);
//
//                },
//              ),
//
//
//
//            ],
//          );
//        });
//  }

  Future<String> forgotRequest() async {


    var bodyre="{\"phone\":${mobile.text}}";


    var response = await http.post(Uri.parse("http://pioneer.redmatter.tech/pioneer/web/forgot_password.php"),
        headers: {"Content-Type": "application/json"},
        body: bodyre
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }
}

