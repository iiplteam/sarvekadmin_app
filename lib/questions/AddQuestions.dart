
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';


import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/models/User.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';
import 'package:sarvekadmin_app/questions/GroupSingleView.dart';

import 'package:shared_preferences/shared_preferences.dart';

class AddQuestionsPage extends StatefulWidget {
  static String tag = 'User-page';
  int surveyindex;
  AddQuestionsPage(this.surveyindex);
  @override
  _AddQuestionsPageState createState() => new _AddQuestionsPageState();
}

class _AddQuestionsPageState extends State<AddQuestionsPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  static final CREATE_USER_URL = Constants.baseUrl+'user';
  TextEditingController emailControler = new TextEditingController();
  TextEditingController usertypeControler = new TextEditingController();
  TextEditingController phoneControler = new TextEditingController();
  TextEditingController nameControler = new TextEditingController();

  SharedPreferences  prefs=null;
  var  userId="";
  TextEditingController mobile=new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  SurveyProvider surveyProvider;
  @override
  void initState() {
    super.initState();
    _getshared_pre();


  }
  @override
  Widget build(BuildContext context) {
    surveyProvider=Provider.of<SurveyProvider>(context);
    final logo = Hero(
      tag: 'hero',
      child: Container(
       // width: MediaQuery.of(context).size.width/2,
       // height: MediaQuery.of(context).size.height/,
        /// Set Background image in splash screen layout (Click to open code)

        child: Image.asset("asserts/logo.png",width: 200,height: 50,),
      ),
    );









    return Scaffold(

        body:  Container(
          color: Colors.white,
           // alignment: Alignment.centerRight,
          height: MediaQuery.of(context).size.height,

            child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left:30.0,top: 30),
                      child: Container(

                          alignment: Alignment.centerRight,
                          child: Row(children: [
                            RaisedButton(
                                onPressed: (){
                                  surveyProvider.addGroup('Group');
                                },
                                child: Text('Add Group',style: TextStyle(),)),
                          ],)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:30.0,top: 30),
                      child: Container(

                          alignment: Alignment.centerRight,
                          child: Row(children: [
                            RaisedButton(
                                onPressed: (){
                                 surveyProvider.questions.forEach((element) {
                                 print("....${element.toJson()}");
                                 });

                                },
                                child: Text('Submit',style: TextStyle(),)),
                          ],)),
                    ),
                  ],
                ),
                Expanded(
                  child: Form(
                    key: _formKey,
                    child:  ListView.builder(
                        shrinkWrap: true, // new line
                        itemCount: surveyProvider.groups.length,
                        itemBuilder: (BuildContext context,int index){
                          return ListTile(
                              leading: Icon(Icons.list),
                              trailing: InkWell(onTap: (){
                                surveyProvider.removeGroup(surveyProvider.groups[index], index);
                              },child: Icon(Icons.delete)),
                              title:Text("${surveyProvider.groups[index]}"),
                            subtitle:  GroupSingleView(surveyProvider.groups[index], widget.surveyindex,index),
                          );
                        }
                    ),
                )
                )]))



        );
  }
  Future<String> createPost(String url, body) async {
    print(body);
    return http.post(Uri.parse(url),headers: {"Content-Type": "application/json"}, body: body).then((http.Response response) {
      final int statusCode = response.statusCode;
      print(response.body);

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return response.body;
    });
  }
  Future<String> apiRequest(String url, String email,String password) async {
    // print(jsonMap);

    var body = json.encode({"name":email,"password":password});

    var response = await http.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: body
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }

  _getshared_pre() async {
    prefs=await SharedPreferences.getInstance();
    userId = prefs.getString("email") ?? '';
  }
  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
    //return null;
  }

//  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context,int index) async {
//    return showDialog<ConfirmAction>(
//        context: context,
//        barrierDismissible: false, // user must tap button for close dialog!
//        builder: (BuildContext context) {
//          return AlertDialog(
//            title: Text('Reset  password'),
//            content: TextFormField(style: TextStyle(fontFamily: 'Gilroy'),
//              controller: mobile,
//              keyboardType: TextInputType.phone,
//              decoration: InputDecoration(
//                  labelText: 'Enter Mobile Number'
//              ),
//            ),
//            actions: <Widget>[
//              FlatButton(
//                padding: EdgeInsets.all(10),
//                child: const Text('OK', style: TextStyle(
//                    backgroundColor: Colors.red,
//                    color: Colors.white,
//                    fontSize: 20,fontFamily: 'Gilroy'),),
//                onPressed: () {
//                  forgotRequest().then((onValue){
//                    if(onValue.contains("true"))
//                      {
//                        Fluttertoast.showToast(msg: "Password sent successful");
//                      }
//                      else
//                        {
//                          Fluttertoast.showToast(msg: "Invalid phone number entered");
//                        }
//                  });
//                  Navigator.pop(context);
//
//                },
//              ),
//
//
//
//            ],
//          );
//        });
//  }

  Future<String> forgotRequest() async {


    var bodyre="{\"phone\":${mobile.text}}";


    var response = await http.post(Uri.parse("http://pioneer.redmatter.tech/pioneer/web/forgot_password.php"),
        headers: {"Content-Type": "application/json"},
        body: bodyre
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }
}

