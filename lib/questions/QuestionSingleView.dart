
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';


import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_tag_editor/tag_editor.dart';
import 'package:provider/provider.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/models/Question.dart';
import 'package:sarvekadmin_app/models/User.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';

import 'package:shared_preferences/shared_preferences.dart';

class QuestionSingleView extends StatefulWidget {
  static String tag = 'User-page';
  final Question question;
  final int index;
  QuestionSingleView(this.question,this.index);
  @override
  _QuestionSingleViewState createState() => new _QuestionSingleViewState();
}

class _QuestionSingleViewState extends State<QuestionSingleView> {
  
  SurveyProvider surveyProvider;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  static final CREATE_SURVEY_URL = Constants.baseUrl+'survey';
  TextEditingController questionControler = new TextEditingController();
  TextEditingController hintControler = new TextEditingController();
bool ismadatary=false;
List<String> values=new List<String>();
  SharedPreferences  prefs=null;
  var  userId="";
  TextEditingController mobile=new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    _getshared_pre();


  }
  @override
  Widget build(BuildContext context) {
    surveyProvider=Provider.of<SurveyProvider>(context);
    return  Container(
      padding: EdgeInsets.all(20),
alignment: Alignment.centerLeft,
      decoration:
      BoxDecoration(border:
      Border.all(color: Colors.grey[600],width: 2),
     // color: Colors.grey
      ),
        // alignment: Alignment.centerRight,
      //  height: MediaQuery.of(context).size.height/3,

        child: Column(
         // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: [
    Container(
      alignment: Alignment.topRight,
      child:  Text('${widget.question.questionType}'.toUpperCase(),style: TextStyle(fontSize: 18,color: Colors.black),),),
        Container(

      alignment: Alignment.topRight,

      child:  InkWell(onTap: (){

        surveyProvider.removeQuestionfromList(widget.index);

        },child: Icon(Icons.delete)),),
  ],
),
            // Padding(
            //   padding: const EdgeInsets.only(left:30.0,top: 30),
            //   child: Container(
            //
            //       alignment: Alignment.centerLeft,
            //       child: logo),
            // ),
            Wrap(
              alignment: WrapAlignment.spaceBetween,
runSpacing: 10,
              children: [
                 Column(
                   crossAxisAlignment: CrossAxisAlignment.start,

                   children: [
                      Container(

                        child: Padding(
                          padding: const EdgeInsets.only(left: 20,right: 20),
                          child: Text('Question Text',style: TextStyle(fontSize: 14,color: Colors.black),),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20,right: 20),
                        child: Container(
                          width: 150,
                          child: TextFormField(
                            controller: questionControler,
                            keyboardType: TextInputType.name,
                            autofocus: true,
                            validator: (s){
                              if(s.trim().isEmpty)
                              {
                                return "Plase enter question Text";
                              }
                              return null;
                            },
                            onChanged: (String s){
                              print('ffffffffffffffffffffffffffff${questionControler.text}');
                              Question q=surveyProvider.questions[widget.index];
                          q.question=questionControler.text;
                            surveyProvider.updateQuestiontoList(q, widget.index);

                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              //   labelText: "Email",

                                hintText: "Enter question Text",
                                hintStyle:TextStyle(color: Colors.black,fontSize: 12) ,
                                isDense: true, // important line
                                contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                    ),
                                    borderRadius: BorderRadius.circular(8)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color:Theme.of(context).focusColor,
                                    ),
                                    borderRadius: BorderRadius.circular(8)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.black))),



                          ),
                        ),
                      ),
                    ],
                  ),



               Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(


                        child: Padding(
                          padding: const EdgeInsets.only(left: 20,right: 20),
                          child: Text('Hint',style: TextStyle(fontSize: 14,color: Colors.black),),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20,right: 20),
                        child: Container(

                          width: 150,
                          child: TextFormField(
                            controller: hintControler,
                            keyboardType: TextInputType.name,
                            autofocus: true,
                            validator: (s){
                              if(s.trim().isEmpty)
                              {
                                return "Plase enter hint";
                              }
                              return null;
                            },
                            onChanged: (String s){
                              print('ffffffffffffffffffffffffffff${hintControler.text}');
                              Question q=surveyProvider.questions[widget.index];
                              q.hint=questionControler.text;
                              surveyProvider.updateQuestiontoList(q, widget.index);

                            },
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              //   labelText: "Email",
                                hintText: "Enter hint text",
                                hintStyle:TextStyle(color: Colors.black,fontSize: 12) ,
                                isDense: true, // important line
                                contentPadding: EdgeInsets.fromLTRB(10, 20, 20, 10),// control your hints text size

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                    ),
                                    borderRadius: BorderRadius.circular(8)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color:Theme.of(context).focusColor,
                                    ),
                                    borderRadius: BorderRadius.circular(8)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.black))),



                          ),
                        ),
                      ),
                    ],
                  ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                     Container(


                      child: Padding(
                        padding: const EdgeInsets.only(left: 20,right: 20),
                        child: Text('Mandatary',style: TextStyle(fontSize: 14,color: Colors.black),),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20,right: 20),
                      child: Checkbox(
                        checkColor: Colors.greenAccent,
                        activeColor: Colors.red,
                        value:ismadatary ,
                        onChanged: (bool value) {
                          setState(() {
                            setState(() {
                              ismadatary=value;

                                print('ffffffffffffffffffffffffffff${ismadatary}');
                                Question q=surveyProvider.questions[widget.index];
                                q.isMandatory=ismadatary;
                                surveyProvider.updateQuestiontoList(q, widget.index);


                            });
                          });
                        },
                      ),
                    ),

                  ],
                ),
                widget.question.questionType=="text"? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    Container(

                      child: Padding(
                        padding: const EdgeInsets.only(left: 20,right: 20),
                        child: Text('Text Type',style: TextStyle(fontSize: 14,color: Colors.black),),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20,right: 20),
                      child: Container(
                        width: 150,
                        alignment: Alignment.center,
                        child: DropdownButton<String>(
                          items: <String>['Text', 'Number', 'Email', 'Phone'].map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (String s) {
                            setState(() {
                              widget.question.validationType=s;
                            });
                          },
                          value: widget.question.validationType,
                        ),
                      ),
                    ),
                  ],
                ):Container(),
                widget.question.questionType=="radio"
                    ||widget.question.questionType=="select"
                    ||widget.question.questionType=="checkbox"?
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(


                      child: Padding(
                        padding: const EdgeInsets.only(left: 20,right: 20),
                        child: Text('Options',style: TextStyle(fontSize: 14,color: Colors.white70),),
                      ),
                    ),
                    Container(
width: 300,
                      padding: const EdgeInsets.only(left: 20,right: 20),

                      child: TagEditor(
                        length: values.length,
                        delimiters: [',', ' '],
                        hasAddButton: true,
                        inputDecoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: 'enter options...',
                        ),
                        onTagChanged: (newValue) {
                          setState(() {
                            values.add(newValue);
                            Question q=surveyProvider.questions[widget.index];
                            q.options=values;
                            surveyProvider.updateQuestiontoList(q, widget.index);

                          });
                        },
                        tagBuilder: (context, index) => _Chip(
                          index: index,
                          label: values[index],
                          onDeleted: _onDelete,
                        ),
                      ),
                    ),

                  ],
                ):Container(),


              ],
            ),




          ],
        ),
      );
  }
  _onDelete(index) {
    setState(() {
      values.removeAt(index);
    });
  }
  Future<String> createPost(String url, body) async {
    print(body);
    return http.post(Uri.parse(url),headers: {"Content-Type": "application/json"}, body: body).then((http.Response response) {
      final int statusCode = response.statusCode;
      print(response.body);

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return response.body;
    });
  }
  Future<String> apiRequest(String url, String email,String password) async {
    // print(jsonMap);

    var body = json.encode({"name":email,"password":password});

    var response = await http.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: body
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }

  _getshared_pre() async {
    prefs=await SharedPreferences.getInstance();
    userId = prefs.getString("email") ?? '';
  }
  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
    //return null;
  }

//  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context,int index) async {
//    return showDialog<ConfirmAction>(
//        context: context,
//        barrierDismissible: false, // user must tap button for close dialog!
//        builder: (BuildContext context) {
//          return AlertDialog(
//            title: Text('Reset  password'),
//            content: TextFormField(style: TextStyle(fontFamily: 'Gilroy'),
//              controller: mobile,
//              keyboardType: TextInputType.phone,
//              decoration: InputDecoration(
//                  labelText: 'Enter Mobile Number'
//              ),
//            ),
//            actions: <Widget>[
//              FlatButton(
//                padding: EdgeInsets.all(10),
//                child: const Text('OK', style: TextStyle(
//                    backgroundColor: Colors.red,
//                    color: Colors.white,
//                    fontSize: 20,fontFamily: 'Gilroy'),),
//                onPressed: () {
//                  forgotRequest().then((onValue){
//                    if(onValue.contains("true"))
//                      {
//                        Fluttertoast.showToast(msg: "Password sent successful");
//                      }
//                      else
//                        {
//                          Fluttertoast.showToast(msg: "Invalid phone number entered");
//                        }
//                  });
//                  Navigator.pop(context);
//
//                },
//              ),
//
//
//
//            ],
//          );
//        });
//  }

  Future<String> forgotRequest() async {


    var bodyre="{\"phone\":${mobile.text}}";


    var response = await http.post(Uri.parse("http://pioneer.redmatter.tech/pioneer/web/forgot_password.php"),
        headers: {"Content-Type": "application/json"},
        body: bodyre
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }

}
class _Chip extends StatelessWidget {
  const _Chip({
    @required this.label,
    @required this.onDeleted,
    @required this.index,
  });

  final String label;
  final ValueChanged<int> onDeleted;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Chip(
      labelPadding: const EdgeInsets.only(left: 8.0),
      label: Text(label),
      deleteIcon: Icon(
        Icons.close,
        size: 18,
      ),
      onDeleted: () {
        onDeleted(index);
      },
    );
  }
}
