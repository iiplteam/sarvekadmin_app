
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';


import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sarvekadmin_app/Demo/SurveyEntryScreen.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/models/User.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';
import 'package:sarvekadmin_app/questions/QuestionSingleView.dart';

import 'package:shared_preferences/shared_preferences.dart';

class GroupSingleView extends StatefulWidget {
  static String tag = 'User-page';
  final String groupName;
  final int surveyindex;
  final int groupindex;
  GroupSingleView(this.groupName,this.surveyindex,this.groupindex);
  @override
  _GroupSingleViewState createState() => new _GroupSingleViewState();
}

class _GroupSingleViewState extends State<GroupSingleView> {
  
  SurveyProvider surveyProvider;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  static final CREATE_SURVEY_URL = Constants.baseUrl+'survey';
  TextEditingController codeControler = new TextEditingController();
  TextEditingController nameControler = new TextEditingController();
  TextEditingController startdateControler = new TextEditingController();
  TextEditingController enddateControler = new TextEditingController();
  String startdate,enddate,status;

  SharedPreferences  prefs=null;
  var  userId="";
  TextEditingController mobile=new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    _getshared_pre();


  }
  @override
  Widget build(BuildContext context) {
    surveyProvider=Provider.of<SurveyProvider>(context);
    return  Container(
      padding: EdgeInsets.all(20),
alignment: Alignment.centerLeft,
      decoration:
      BoxDecoration(border:
      Border.all(color: Colors.grey[600],width: 2)),
        // alignment: Alignment.centerRight,
      //  height: MediaQuery.of(context).size.height/3,

        child: SingleChildScrollView(
          child: Column(
           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
SizedBox(height: 20,),
              Container(
                alignment:Alignment.centerLeft,

                child: Text('${surveyProvider.surveyList[widget.surveyindex].surveyName}',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),),
              surveyProvider.questions.length>0?Padding(
                padding: const EdgeInsets.only(left:30.0,top: 30),
                child: Container(

                    alignment: Alignment.centerRight,
                    child:
                      RaisedButton(
                          onPressed: (){
                            print(surveyProvider.questions);
                            surveyProvider.questions.forEach((element) {
                              print("....${element.toJson()}");
                            });
                            surveyProvider.createQuestions(surveyProvider.surveyList[widget.surveyindex].surveyId).then((value){
                              if(value)
                                {
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>SurveyEntryScreen()));

                                }else
                                  {
                                    Fluttertoast.showToast(msg: 'Create questions faild');
                                  }
                            });
                          },
                          child: Text('Submit',style: TextStyle(),)),
                    ),
              ):Container(),
              // Padding(
              //   padding: const EdgeInsets.only(left:30.0,top: 30),
              //   child: Container(
              //
              //       alignment: Alignment.centerLeft,
              //       child: logo),
              // ),
               Wrap(


                    spacing: 5,
                    children: <Widget>[
                      InkWell(
                        onTap: (){
                          surveyProvider.addQuestiontoList('text',widget.groupName,widget.surveyindex);
                        },
                        child: Container(

                          child: Text('Text',style: TextStyle(color: Colors.black),),
                          decoration:
                          BoxDecoration(border:
                          Border.all(color: Theme.of(context).focusColor,width: 2)),
                          padding: EdgeInsets.all(5),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          surveyProvider.addQuestiontoList('textarea',widget.groupName,widget.groupindex);

                        },
                        child: Container(

                          child: Text('TextArea',style: TextStyle(color: Colors.black)),
                          decoration:
                          BoxDecoration(border:
                          Border.all(color: Theme.of(context).focusColor,width: 2)),
                          padding: EdgeInsets.all(5),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          surveyProvider.addQuestiontoList('radio',widget.groupName,widget.groupindex);

                        },
                        child: Container(

                          child: Text('Radio',style: TextStyle(color: Colors.black)),
                          decoration:
                          BoxDecoration(border:
                          Border.all(color: Theme.of(context).focusColor,width: 2)),
                          padding: EdgeInsets.all(5),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          surveyProvider.addQuestiontoList('select',widget.groupName,widget.groupindex);

                        },
                        child: Container(

                          child: Text('Select',style: TextStyle(color: Colors.black)),
                          decoration:
                          BoxDecoration(border:
                          Border.all(color: Theme.of(context).focusColor,width: 2)),
                          padding: EdgeInsets.all(5),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          surveyProvider.addQuestiontoList('checkbox',widget.groupName,widget.groupindex);

                        },
                        child: Container(

                          child: Text('CheckBox',style: TextStyle(color: Colors.black)),
                          decoration:
                          BoxDecoration(border:
                          Border.all(color: Theme.of(context).focusColor,width: 2)),
                          padding: EdgeInsets.all(5),
                        ),
                      ),
                    ],
                  ),
              ListView(
                 shrinkWrap: true, // new line
                 physics: NeverScrollableScrollPhysics(),
                 children: List.generate(surveyProvider.questions.length, (index){
                 return surveyProvider.questions[index].groupName==widget.groupName?QuestionSingleView(surveyProvider.questions[index], index):Container();
               }),
             ),
            ],
          ),
        ),
      );
  }
  Future<String> createPost(String url, body) async {
    print(body);
    return http.post(Uri.parse(url),headers: {"Content-Type": "application/json"}, body: body).then((http.Response response) {
      final int statusCode = response.statusCode;
      print(response.body);

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return response.body;
    });
  }
  Future<String> apiRequest(String url, String email,String password) async {
    // print(jsonMap);

    var body = json.encode({"name":email,"password":password});

    var response = await http.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: body
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }

  _getshared_pre() async {
    prefs=await SharedPreferences.getInstance();
    userId = prefs.getString("email") ?? '';
  }
  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
    //return null;
  }

//  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context,int index) async {
//    return showDialog<ConfirmAction>(
//        context: context,
//        barrierDismissible: false, // user must tap button for close dialog!
//        builder: (BuildContext context) {
//          return AlertDialog(
//            title: Text('Reset  password'),
//            content: TextFormField(style: TextStyle(fontFamily: 'Gilroy'),
//              controller: mobile,
//              keyboardType: TextInputType.phone,
//              decoration: InputDecoration(
//                  labelText: 'Enter Mobile Number'
//              ),
//            ),
//            actions: <Widget>[
//              FlatButton(
//                padding: EdgeInsets.all(10),
//                child: const Text('OK', style: TextStyle(
//                    backgroundColor: Colors.red,
//                    color: Colors.white,
//                    fontSize: 20,fontFamily: 'Gilroy'),),
//                onPressed: () {
//                  forgotRequest().then((onValue){
//                    if(onValue.contains("true"))
//                      {
//                        Fluttertoast.showToast(msg: "Password sent successful");
//                      }
//                      else
//                        {
//                          Fluttertoast.showToast(msg: "Invalid phone number entered");
//                        }
//                  });
//                  Navigator.pop(context);
//
//                },
//              ),
//
//
//
//            ],
//          );
//        });
//  }

  Future<String> forgotRequest() async {


    var bodyre="{\"phone\":${mobile.text}}";


    var response = await http.post(Uri.parse("http://pioneer.redmatter.tech/pioneer/web/forgot_password.php"),
        headers: {"Content-Type": "application/json"},
        body: bodyre
    );
    print("${response.statusCode}");
    print("${response.body}");

    return response.body.toString();
  }
}

