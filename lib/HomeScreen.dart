
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';




import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:sarvekadmin_app/AppDrawer.dart';
import 'package:sarvekadmin_app/Demo/DemoScreen.dart';
import 'package:sarvekadmin_app/models/Survey.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';
import 'package:sarvekadmin_app/providers/SurveyProvider.dart';
import 'package:sarvekadmin_app/questions/AddQuestions.dart';
import 'package:sarvekadmin_app/surveypages/AddSurvey.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

import 'Demo/formApi.dart';



//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';








class HomeScreen  extends StatefulWidget
{
  int count;

  @override
  _HomeScreenState createState() => _HomeScreenState();

  void onItemSelected(int position) {
    Fluttertoast.showToast(msg: "your select $position");

  }
}



class _HomeScreenState extends State<HomeScreen>  with WidgetsBindingObserver {
  SharedPreferences prefs = null;
  final GlobalKey _scaffoldKey = new GlobalKey();
  SurveyProvider surveyProvider;
  bool intialdataload=true;
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    print("Activity Resumed ");


  }


  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("Activity Resumed $state");
    switch (state.index) {
      case 0: // resumed
      //datasyncwithserver();
        break;
      case 1: // inactive

        break;
      case 2: // paused

        break;
    }
  }
loadData(){
    if(intialdataload) {
      surveyProvider.getSurveys().then((value) {
        setState(() {

        });
      }

      );
      setState(() {
        intialdataload=false;
      });
    }
}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    surveyProvider=Provider.of<SurveyProvider>(context);
    loadData();
    return Scaffold(
key: _scaffoldKey,
drawer:
 AppDrawer(),
      endDrawer: AddSurveyPage(),


      body: ScreenTypeLayout(
          mobile: body(context),

      desktop:Row(children: [
        SizedBox(
            width: 300,
            child: AppDrawer(selectedIndex: 0,)),
        Expanded(child: body(context))
      ],),

    ),
    );
  }

  Widget body(BuildContext context1){
    return Builder(
        builder: (context) =>Column(
      children: [
        SizedBox(height: 50,),
        Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Active Surveys",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.grey[600]),),
              Row(
                children: [
                  Text("Create"),
                  SizedBox(width: 20,),
                  InkWell(onTap: (){
                    Scaffold.of(context).openEndDrawer();
                  }, child: Icon(Icons.add_circle,color:Theme.of(context).focusColor,))
                ],)
            ],),
        ),
        SizedBox(height: 50,),
        surveyProvider.surveyList.length>0?Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left:20.0),
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 280,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: surveyProvider.surveyList.length,
                itemBuilder: (BuildContext ctx, index) {
                  Survey surveyiteam=surveyProvider.surveyList[index];
                  return  Container(
                    padding: EdgeInsets.all(10),
                    height: 310,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(22),),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 1.0,
                          spreadRadius: 0.0,
                          offset: Offset(0.0, 1.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child:  Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("${surveyiteam.surveyName}", style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                            // InkWell(onTap:(){
                            //   myPopMenu();
                            // },child: Icon(Icons.workspaces_filled,color: Colors.grey,))
                            myPopMenu(index)
                          ],
                        ),
                        //  SizedBox(height: 10,),
                        Text("Please write a description about your survey",style: TextStyle(color: Colors.grey,fontSize: 12),),

                        Container(
                          alignment: Alignment.centerLeft,
                          child: Container(
                              padding: EdgeInsets.all(8),
                              child: Text('Reports',style: TextStyle(color: Theme.of(context).focusColor),),
                              decoration: BoxDecoration(

                                borderRadius: BorderRadius.all(Radius.circular(20),),
                                color: Color(0xFFFEF0E8),)),
                        )
                      ],
                    ),


                  );
                }),
          ),
        ):CircularProgressIndicator(),
      ],
    ));
  }

  Widget myPopMenu(int index) {
    return PopupMenuButton(
        onSelected: (value) {

          Fluttertoast.showToast(
              msg: "You have selected " + value.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.black,
              textColor: Colors.white,
              fontSize: 16.0
          );
          if(value==1)
          Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DemoScreen(index)));
          else
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FormApi(survekId:index ,)));

        },
        itemBuilder: (context) => [
          PopupMenuItem(
              value: 1,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                    child: Icon(Icons.question_answer),
                  ),
                  Text('Add Questions')
                ],
              )),
          PopupMenuItem(
              value: 2,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                    child: Icon(Icons.message),
                  ),
                  Text('CRM')
                ],
              )),
          // PopupMenuItem(
          //     value: 3,
          //     child: Row(
          //       children: <Widget>[
          //         Padding(
          //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
          //           child: Icon(Icons.add_circle),
          //         ),
          //         Text('Add')
          //       ],
          //     )),
        ]);
  }
}








