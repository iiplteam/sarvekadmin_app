
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sarvekadmin_app/HomeScreen.dart';
import 'package:sarvekadmin_app/TemplatesScreen.dart';

import 'package:shared_preferences/shared_preferences.dart';



class AppDrawer extends StatefulWidget {
  int selectedIndex;
AppDrawer({this.selectedIndex=0});
  @override
  _AppDrawerState createState() => new _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
 // LoginProvider loginProvider;
  bool isLoading = true;
  bool isExpand = false;
  String versionname = '';
  loadData() async {
    if (isLoading == true) {
      // GetVersion.projectVersion.then((value) {
      //   setState(() {
      //     versionname = value;
      //   });
      // });
      // isLoading = false;
      // loginProvider.getUserId().then((value) {
      //   if (value) {
      //     setState(() {});
      //   }

      // });
    }
  }

  @override
  Widget build(BuildContext context) {
    //loginProvider = Provider.of<LoginProvider>(context);
    loadData();

    return Drawer(
      child: Container(

        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('asserts/bg.png',),fit: BoxFit.cover)),
      alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.only(left:18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _createHeader(),
             Center(
               child: ListTile(
                    leading: Container(

                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          shape: BoxShape.circle
                      ),
                    ),
                    title: Text("Rohit.T",style: TextStyle(fontSize: 16,color: Theme.of(context).focusColor),)
                    ,subtitle: Text("Profile",style: TextStyle(fontSize: 12,color: Colors.grey)),),
             ),

              SizedBox(height: 50,),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>HomeScreen()));
            },
            child:Container(
                //color: Colors.white60,
                  child: ListTile(
                    leading: Icon(Icons.settings,color: widget.selectedIndex==0?Colors.white:Colors.white60,),
                      title: Text("Surveys",style: TextStyle(fontSize: 12,color: widget.selectedIndex==0?Colors.white:Colors.white60),)
              ))),
              SizedBox(height: 20,),
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>TemplatesScreen()));
                },
                child: Container(
                    child: ListTile(
                        leading: Icon(Icons.sticky_note_2_rounded,color:widget.selectedIndex==1?Colors.white: Colors.white60,),
                        title: Text("Templates",style: TextStyle(fontSize: 12,color: widget.selectedIndex==1?Colors.white:Colors.white60),)
                    )),
              ),
              SizedBox(height: 20,),
              Container(
                  child: ListTile(
                      leading: Icon(Icons.group,color:widget.selectedIndex==2?Colors.white: Colors.white60,),
                      title: Text("Users",style: TextStyle(fontSize: 12,color: widget.selectedIndex==2?Colors.white:Colors.white60),)
                  )),
              SizedBox(height: 20,),
              Container(
                  child: ListTile(
                      leading: Icon(Icons.email_outlined,color: widget.selectedIndex==3?Colors.white:Colors.white60,),
                      title: Text("CRM",style: TextStyle(fontSize: 12,color:widget.selectedIndex==3?Colors.white: Colors.white60),)
                  )),
              SizedBox(height: 20,),
              Container(
                  child: ListTile(
                      leading: Icon(Icons.settings,color: widget.selectedIndex==4?Colors.white:Colors.white60,),
                      title: Text("Settings",style: TextStyle(fontSize: 12,color:widget.selectedIndex==4?Colors.white: Colors.white60),)
                  )),
              SizedBox(height: 20,),
            ],
          ),
        ),
      ),
    );
  }


  Widget _createHeader() {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        // decoration: BoxDecoration(
        //   gradient: LinearGradient(
        //       colors: [
        //         const Color(0xFFff8400),
        //         const Color(0xFFff8400),
        //       ],
        //       begin: const FractionalOffset(0.0, 0.0),
        //       end: const FractionalOffset(1.0, 0.0),
        //       stops: [0.0, 1.0],
        //       tileMode: TileMode.clamp),
        // ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[
            InkWell(
              onTap: () {
               // Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> bottomNavigationBar(currentIndex: 0,)));

              },
              child: Image.asset(
                "asserts/logo.png",
                height: 50,
              ),
            ),

//            Text(
//                "${loginProvider.userData != null ? loginProvider.userData.name : ''}",
//                style: TextStyle(
//                    color: Colors.white,
//                    fontSize: 18.0,
//                    fontWeight: FontWeight.w500)),
//            SizedBox(
//              width: 20,
//            ),
//            Text(
//                "${loginProvider.userData != null ? loginProvider.userData.phone : ''}",
//                style: TextStyle(
//                    color: Colors.white,
//                    fontSize: 18.0,
//                    fontWeight: FontWeight.w500)),
          ],
        ));
  }
}
