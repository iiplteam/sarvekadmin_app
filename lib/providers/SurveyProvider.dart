import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'dart:io';
//import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:sarvekadmin_app/models/Answer.dart';
import 'package:sarvekadmin_app/models/Question.dart';
import 'package:sarvekadmin_app/models/Questiononline.dart';
import 'package:sarvekadmin_app/models/Survey.dart';
import 'package:sarvekadmin_app/providers/Constants.dart';

import 'package:shared_preferences/shared_preferences.dart';

class SurveyProvider with ChangeNotifier {
  static final CREATE_SURVEY_URL = Constants.baseUrl+'survey';
  static final CREATE_questions_URL = Constants.baseUrl+'survey-question';
  static final CREATE_ANSWER_URL = Constants.baseUrl+'survey-answer';
  //
  var toastMessage = {"status": true, "message": ""};
String token;
  List<Survey> surveyList=new List<Survey>();
List<String> groups=new List<String>();
List<Question> questions=new List<Question>();
  List<Questiononline> questionsbysurvey=new List<Questiononline>();

  List<Answer> answers=new List<Answer>();
  Map<int,Answer> answermap=new HashMap<int,Answer>();
  void toastChange({status: bool, message}) {
    toastMessage = {"status": status, "message": message};
    print("toastMessage $toastMessage");
    notifyListeners();
  }

  void changeUserData() {

    notifyListeners();
  }

  Future<void> addGroup(String name){
groups.add("Group${groups.length+1}");
notifyListeners();

  }
  Future<void> removeGroup(String name,int index){
    groups.removeAt(index);
    notifyListeners();

  }
  Future<void> addQuestiontoList(String type,groupname,index){
   Question question=new Question();
   question.groupName=groupname;
   question.questionType=type;
   question.surveyCode=surveyList[index].surveyId;
    questions.add(question);
    notifyListeners();

  }
  Future<void> updateQuestiontoList(Question question,int index){

    questions[index]=question;
    notifyListeners();

  }
  Future<void> removeQuestionfromList(int index){
    questions.removeAt(index);
    notifyListeners();

  }

  void addAnswer(int index,String value)
  {
    Answer a=new Answer();
    print('vvvvvv$value');
    a.value=value;
    a.surveyCode=questionsbysurvey[index].surveyCode;
    a.questionCode=questionsbysurvey[index].questionId;
    a.question=questionsbysurvey[index].question;
    a.valueType=questionsbysurvey[index].validationType;
    answermap[index]=a;
    print(answermap[index].value);
    notifyListeners();

  }

  Future<bool> createSurvey(Survey survey) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization' :"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDgxNjUyMTI1NiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgyMjIyMn0.GranSJeDYtwgvfW0oZuMf-ZBKQNbQWjEWv8YHZa5Ia8"
    };

    var request = http.Request('POST', Uri.parse(CREATE_SURVEY_URL));
    request.body = '''{\r\n    "surveyName" : "${survey.surveyName}",\r\n    "surveyCode":  "${survey.surveyCode}",\r\n    "startDate": "${survey.startDate}",\r\n    "endDate" : "${survey.endDate}",\r\n   "description" : "${survey.description}",\r\n    "group" : "Basic",\r\n    "isPublic": ${survey.isPublic}\r\n}''';
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    print(response.statusCode);
    var resp=json.decode(await response.stream.bytesToString());
    print(resp);
    if (response.statusCode == 201) {

      print("survey respone--------$resp");
      getSurveys();
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }

  }
  Future<bool> getSurveys() async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization' :"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDgxNjUyMTI1NiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgyMjIyMn0.GranSJeDYtwgvfW0oZuMf-ZBKQNbQWjEWv8YHZa5Ia8"
    };
surveyList=new List<Survey>();
    var request = http.Request('GET', Uri.parse(CREATE_SURVEY_URL),);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    print(response.statusCode);
    var resp=json.decode(await response.stream.bytesToString());
    print(resp);
    if (response.statusCode == 200) {

      print("survey respone--------$resp['data']");
      for(var i=0;i<resp['data'].length;i++)
      {
        print("single value----${resp['data'][i]['created_on']}");
        surveyList.add(Survey.fromJson(resp['data'][i]));
      }



      print("survey count--------${surveyList.length}");
      notifyListeners();
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }


  }
  Future<bool> createQuestions(String surveyid) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization' :"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDgxNjUyMTI1NiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgyMjIyMn0.GranSJeDYtwgvfW0oZuMf-ZBKQNbQWjEWv8YHZa5Ia8"
    };

    var request = http.Request('POST', Uri.parse(CREATE_questions_URL+'/$surveyid'));
    request.body = '''{\r\n    "questions" : ${json.encode(questions)} }''';
    request.headers.addAll(headers);
print(request.body);
    http.StreamedResponse response = await request.send();
    print(response.statusCode);
    var resp=json.decode(await response.stream.bytesToString());
    print(resp);
    if (response.statusCode == 201) {

      print("questions respone--------$resp");
      getSurveys();
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }

  }
  Future<bool> getQuestions(String surveyid) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization' :"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDgxNjUyMTI1NiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgyMjIyMn0.GranSJeDYtwgvfW0oZuMf-ZBKQNbQWjEWv8YHZa5Ia8"
    };
    questionsbysurvey=new List<Questiononline>();
    var request = http.Request('GET', Uri.parse(CREATE_questions_URL+'/$surveyid'),);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    print(response.statusCode);
    var resp=json.decode(await response.stream.bytesToString());
    print(resp);
    if (response.statusCode == 200) {

      print("questions respone--------$resp['questions']");
      for(var i=0;i<resp['questions'].length;i++)
      {
       // print("single value----${resp['data'][i]['created_on']}");
        questionsbysurvey.add(Questiononline.fromJson(resp['questions'][i]));
      }



      print("questions count--------${questionsbysurvey.length}");
      notifyListeners();
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }


  }
  Future<bool> createAnswer(String surveyid,List<Answer> answers) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization' :"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDA4ZjRjYTMtNmI5OS00ZmVlLTlhODYtZDc5ZDVlNTdlNDMxIiwicm9sZSI6ImFkbWluIiwiaXNfZGVsZXRlIjpmYWxzZSwidXBkYXRlZF9vbiI6MTYyMDgxNjUyMTI1NiwiZW1haWwiOiJzcmtAaWlwbC53b3JrIiwibmFtZSI6InNyayIsImNyZWF0ZWRfb24iOjE2MjA0ODI3MTE5MTksImlhdCI6MTYyMDgyMjIyMn0.GranSJeDYtwgvfW0oZuMf-ZBKQNbQWjEWv8YHZa5Ia8"
    };

    var request = http.Request('POST', Uri.parse(CREATE_ANSWER_URL+'/$surveyid'));
    request.body = '''{\r\n    "answers" : ${json.encode(answers)} }''';
    request.headers.addAll(headers);
    print(request.body);
    http.StreamedResponse response = await request.send();
    print(response.statusCode);
    var resp=json.decode(await response.stream.bytesToString());
    print(resp);
    if (response.statusCode == 200) {

      print("answers respone--------$resp");

      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }

  }
}
